FROM golang:latest as builder
LABEL maintainer="orensimple"
COPY . /
WORKDIR /
ENV GO111MODULE=on
RUN CGO_ENABLED=0 GOOS=linux make build


######## Start a new stage #######
FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /bin/api .
COPY --from=builder /config/config.yml ./config/config.yml
EXPOSE 8088

CMD ["/root/api"]
