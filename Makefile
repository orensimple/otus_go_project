# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
SOURCE_NAME=./api/main.go
BINARY_NAME=bin/api

all: build test test-race
deps:
	go mod download
build: deps build-api
build-api:
	CGO_ENABLED=0 GOOS=linux $(GOBUILD) -o $(BINARY_NAME) $(SOURCE_NAME)
test:
	$(GOTEST) -v pkg/domain/services/choice_test.go
test-race:
	$(GOTEST) -race pkg/domain/services/choice_test.go
clean:
	$(GOCLEAN)
	rm -f $(BINARY_NAME)
run:
	./$(BINARY_NAME)  http_server --config ./config
