module otus_go_project

go 1.13

require (
	github.com/amitrai48/logger v0.0.0-20190214092904-448001c055ec
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/jmoiron/sqlx v1.2.0
	github.com/jstemmer/go-junit-report v0.9.1 // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.6.2
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	github.com/stretchr/testify v1.4.0
	go.uber.org/zap v1.13.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
