package main

import (
	"context"
	"log"
	"net/http"
	"strings"
	"time"

	"otus_go_project/config"
	"otus_go_project/pkg/domain/handlers"
	"otus_go_project/pkg/domain/mw"
	"otus_go_project/pkg/domain/services"
	"otus_go_project/pkg/logger"
	"otus_go_project/pkg/storage"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const (
	CtxTimeOutConst               = 360 * time.Millisecond
	ReportUpdateTickerTimeSeconds = 5
	HTTPServerReadTimeoutSeconds  = 10
	HTTPServerWriteTimeoutSeconds = 10
	HTTPServerMaxReaderBytes      = 1 << 20
)

func main() {
	var addr string

	var RootCmd = &cobra.Command{
		Use:   "http_server",
		Short: "Run http server",
		Run: func(cmd *cobra.Command, args []string) {
			err := config.Init(addr)
			if err != nil {
				logger.ContextLogger.Errorf("Eror init config, viper.ReadInConfig", err.Error())
			}

			logger.InitLogger()

			var serverAddr strings.Builder
			serverAddr.WriteString(viper.GetString("http_listen.ip"))
			serverAddr.WriteString(":")
			serverAddr.WriteString(viper.GetString("http_listen.port"))

			logger.ContextLogger.Infof("Starting http server",
				viper.GetString("http_listen.ip"),
				viper.GetString("http_listen.port"),
			)
			router := &handlers.Handler{}
			mux := http.NewServeMux()

			storage := &storage.Enabled{}
			storage.InitStorages()
			LoadDataFromDbToMem(storage)
			router.Storages = storage
			InitHandlers(mux, router)
			AutoUpdateReports(storage.MainReportService)

			server := &http.Server{
				Addr:           serverAddr.String(),
				Handler:        mux,
				ReadTimeout:    HTTPServerReadTimeoutSeconds * time.Second,
				WriteTimeout:   HTTPServerWriteTimeoutSeconds * time.Second,
				MaxHeaderBytes: HTTPServerMaxReaderBytes,
			}
			err = server.ListenAndServe()
			if err != nil {
				logger.ContextLogger.Errorf("Starting http server", err.Error())
			}
		},
	}

	RootCmd.Flags().StringVar(&addr, "config", "./config", "")

	if err := RootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

func InitHandlers(mux *http.ServeMux, router *handlers.Handler) {
	mux.HandleFunc("/banner/set", mw.HTTPLogger(mw.WithTimeout(router.SetBanner, CtxTimeOutConst)))
	mux.HandleFunc("/banners/get", mw.HTTPLogger(mw.WithTimeout(router.GetBanners, CtxTimeOutConst)))
	mux.HandleFunc("/banner/update", mw.HTTPLogger(mw.WithTimeout(router.UpdateBanner, CtxTimeOutConst)))
	mux.HandleFunc("/banner/delete", mw.HTTPLogger(mw.WithTimeout(router.DeleteBanner, CtxTimeOutConst)))

	mux.HandleFunc("/rotation/set", mw.HTTPLogger(mw.WithTimeout(router.SetRotation, CtxTimeOutConst)))
	mux.HandleFunc("/rotations/get", mw.HTTPLogger(mw.WithTimeout(router.GetRotations, CtxTimeOutConst)))
	mux.HandleFunc("/rotation/update", mw.HTTPLogger(mw.WithTimeout(router.UpdateRotation, CtxTimeOutConst)))
	mux.HandleFunc("/rotation/delete", mw.HTTPLogger(mw.WithTimeout(router.DeleteRotation, CtxTimeOutConst)))

	mux.HandleFunc("/slot/set", mw.HTTPLogger(mw.WithTimeout(router.SetSlot, CtxTimeOutConst)))
	mux.HandleFunc("/slots/get", mw.HTTPLogger(mw.WithTimeout(router.GetSlots, CtxTimeOutConst)))
	mux.HandleFunc("/slot/update", mw.HTTPLogger(mw.WithTimeout(router.UpdateSlot, CtxTimeOutConst)))
	mux.HandleFunc("/slot/delete", mw.HTTPLogger(mw.WithTimeout(router.DeleteSlot, CtxTimeOutConst)))

	mux.HandleFunc("/group/set", mw.HTTPLogger(mw.WithTimeout(router.SetGroup, CtxTimeOutConst)))
	mux.HandleFunc("/groups/get", mw.HTTPLogger(mw.WithTimeout(router.GetGroups, CtxTimeOutConst)))
	mux.HandleFunc("/group/update", mw.HTTPLogger(mw.WithTimeout(router.UpdateGroup, CtxTimeOutConst)))
	mux.HandleFunc("/group/delete", mw.HTTPLogger(mw.WithTimeout(router.DeleteGroup, CtxTimeOutConst)))

	mux.HandleFunc("/report/set", mw.HTTPLogger(mw.WithTimeout(router.SetReport, CtxTimeOutConst)))
	mux.HandleFunc("/reports/get", mw.HTTPLogger(mw.WithTimeout(router.GetReports, CtxTimeOutConst)))
	mux.HandleFunc("/report/update", mw.HTTPLogger(mw.WithTimeout(router.UpdateReport, CtxTimeOutConst)))
	mux.HandleFunc("/report/delete", mw.HTTPLogger(mw.WithTimeout(router.DeleteReport, CtxTimeOutConst)))

	mux.HandleFunc("/choice/get", mw.HTTPLogger(mw.WithTimeout(router.GetChoice, CtxTimeOutConst)))
	mux.HandleFunc("/choice/click", mw.HTTPLogger(mw.WithTimeout(router.ClickChoice, CtxTimeOutConst)))
}

func AutoUpdateReports(service *services.ReportService) {
	logger.ContextLogger.Infof("Start auto update reports")

	go func() {
		uptimeTicker := time.NewTicker(ReportUpdateTickerTimeSeconds * time.Second)

		for {
			select {
			case <-uptimeTicker.C:
				err := service.AutoUpdateRotations(context.Background())
				if err != nil {
					logger.ContextLogger.Errorf("Failed update reports in db", err.Error())
				}
			default:
				continue
			}
		}
	}()
}

func LoadDataFromDbToMem(storage *storage.Enabled) {
	err := storage.MainRotationService.LoadRotationsFromDbToMem()
	if err != nil {
		logger.ContextLogger.Errorf("Problem load rotations from db to memory", err.Error())
	}

	err = storage.MainReportService.LoadReportsFromDbToMem()
	if err != nil {
		logger.ContextLogger.Errorf("Problem load reports from db to memory", err.Error())
	}
}
