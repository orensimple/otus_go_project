//nolint:dupl
package maindb

import (
	"otus_go_project/pkg/logger"
	"time"

	// Postrges driver
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
)

var DbConnect *sqlx.DB //nolint:gochecknoglobals

const RetryConnectionTimeSeconds = 30

func InitDbConnect(dsn string) (err error) {
	DbConnect, err = sqlx.Open("pgx", dsn)
	if err != nil {
		logger.ContextLogger.Errorf("Failed to connect to DB, retry", err.Error())

		timer1 := time.NewTimer(RetryConnectionTimeSeconds * time.Second)
		<-timer1.C

		DbConnect, err = sqlx.Open("pgx", dsn)
		if err != nil {
			logger.ContextLogger.Errorf("Failed to retry connect to DB", dsn, err.Error())
		}
	}

	err = DbConnect.Ping()
	if err != nil {
		logger.ContextLogger.Errorf("Failed to check connection to DB, retry", err.Error())

		timer1 := time.NewTimer(RetryConnectionTimeSeconds * time.Second)
		<-timer1.C

		DbConnect, err = sqlx.Open("pgx", dsn)
		if err != nil {
			logger.ContextLogger.Errorf("Failed to check connection to DB", dsn, err.Error())
		}
	}

	return nil
}
