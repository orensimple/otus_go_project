//nolint:dupl
package maindb

import (
	"context"

	"otus_go_project/pkg/domain/models"
	"otus_go_project/pkg/logger"

	// Postrges driver
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
)

type PgReportStorage struct {
	db *sqlx.DB
}

func NewPgReportStorage() *PgReportStorage {
	return &PgReportStorage{db: DbConnect}
}

func (pges *PgReportStorage) SetReport(ctx context.Context, report *models.Report) error {
	query := `
		INSERT INTO reports(banner_id, group_id, slot_id, show, conversion)
		VALUES (:banner_id, :group_id, slot_id, :show, :conversion)
	`
	_, err := pges.db.NamedExecContext(ctx, query, map[string]interface{}{
		"banner_id":  report.BannerID,
		"group_id":   report.GroupID,
		"slot_id":    report.SlotID,
		"show":       report.Show,
		"conversion": report.Conversion,
	})

	return err
}

func (pges *PgReportStorage) UpdateReport(ctx context.Context, report *models.Report) (*models.Report, error) {
	query := `
		UPDATE reports
		SET show = :show, conversion = :conversion
		WHERE banner_id = :banner_ID AND group_id = : group_id AND slot_id = : slot_id
	`
	_, err := pges.db.NamedExecContext(ctx, query, map[string]interface{}{
		"banner_id":  report.BannerID,
		"group_id":   report.GroupID,
		"slot_id":    report.SlotID,
		"show":       report.Show,
		"conversion": report.Conversion,
	})

	return report, err
}

func (pges *PgReportStorage) GetReports(ctx context.Context) ([]*models.Report, error) {
	query := `
		SELECT slot_id, group_id, banner_id, show, conversion FROM reports
	`

	rows, err := pges.db.QueryContext(ctx, query)
	if err != nil {
		logger.ContextLogger.Infof("QueryContext", "err", err.Error())
	}
	defer rows.Close()

	var reports []*models.Report

	for rows.Next() {
		var report models.Report
		if err := rows.Scan(&report.SlotID, &report.GroupID, &report.BannerID, &report.Show, &report.Conversion); err != nil {
			logger.ContextLogger.Infof("rowScan", "err", err.Error())
		}

		reports = append(reports, &report)
	}

	if err := rows.Err(); err != nil {
		logger.ContextLogger.Infof("rowErr", "err", err.Error())
	}

	return reports, nil
}

func (pges *PgReportStorage) DeleteReport(ctx context.Context, bannerID int64, groupID int64, slotID int64) error {
	query := `
		DELETE FROM reports
		WHERE banner_id = :banner_id AND group_id = : group_id AND slot_id = : slot_id
	`
	_, err := pges.db.NamedExecContext(ctx, query, map[string]interface{}{
		"banner_id": bannerID,
		"group_id":  groupID,
		"slot_id":   slotID,
	})

	return err
}

func (pges *PgReportStorage) AutoUpdateReports(ctx context.Context, reports []*models.Report) error {
	if len(reports) > 0 {
		for _, report := range reports {
			query := `
			INSERT INTO reports (banner_id, group_id, slot_id, show, conversion)
			VALUES(:banner_id, :group_id, :slot_id, :show, :conversion) 
			ON CONFLICT (banner_id, group_id, slot_id) DO UPDATE
				SET show = EXCLUDED.show,
					conversion = EXCLUDED.conversion;
			`
			_, err := pges.db.NamedExecContext(ctx, query, map[string]interface{}{
				"banner_id":  report.BannerID,
				"group_id":   report.GroupID,
				"slot_id":    report.SlotID,
				"show":       report.Show,
				"conversion": report.Conversion,
			})

			if err != nil {
				return err
			}
		}
	}

	return nil
}
