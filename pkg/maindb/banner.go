//nolint:dupl
package maindb

import (
	"context"

	"otus_go_project/pkg/domain/models"
	"otus_go_project/pkg/logger"

	// Postrges driver
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
)

type PgBannerStorage struct {
	db *sqlx.DB
}

func NewPgBannerStorage() *PgBannerStorage {
	return &PgBannerStorage{db: DbConnect}
}

func (pges *PgBannerStorage) SetBanner(ctx context.Context, banner *models.Banner) error {
	query := `
		INSERT INTO banners(id, title)
		VALUES (:id, :title)
	`
	_, err := pges.db.NamedExecContext(ctx, query, map[string]interface{}{
		"id":    banner.ID,
		"title": banner.Title,
	})

	return err
}

func (pges *PgBannerStorage) GetBanners(ctx context.Context) ([]*models.Banner, error) {
	query := `
		SELECT id, title FROM banners
	`

	rows, err := pges.db.QueryContext(ctx, query)
	if err != nil {
		logger.ContextLogger.Infof("QueryContext", "err", err.Error())
	}
	defer rows.Close()

	var banners []*models.Banner

	for rows.Next() {
		var banner models.Banner
		if err := rows.Scan(&banner.ID, &banner.Title); err != nil {
			logger.ContextLogger.Infof("rowScan", "err", err.Error())
		}

		banners = append(banners, &banner)
	}

	if err := rows.Err(); err != nil {
		logger.ContextLogger.Infof("rowErr", "err", err.Error())
	}

	return banners, nil
}

func (pges *PgBannerStorage) UpdateBanner(ctx context.Context, banner *models.Banner) (*models.Banner, error) {
	query := `
		UPDATE banners
		SET title = :title
		WHERE id = :id
	`

	_, err := pges.db.NamedExecContext(ctx, query, map[string]interface{}{
		"id":    banner.ID,
		"title": banner.Title,
	})

	return banner, err
}

func (pges *PgBannerStorage) DeleteBanner(ctx context.Context, id int64) error {
	query := `
		DELETE FROM banners
		WHERE id = :id
	`
	_, err := pges.db.NamedExecContext(ctx, query, map[string]interface{}{
		"id": id,
	})

	return err
}
