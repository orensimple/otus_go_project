//nolint:dupl
package maindb

import (
	"context"

	"otus_go_project/pkg/domain/models"
	"otus_go_project/pkg/logger"

	// Postrges driver
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
)

// implements domain.interfaces.SlotStorage
type PgSlotStorage struct {
	db *sqlx.DB
}

func NewPgSlotStorage() *PgSlotStorage {
	return &PgSlotStorage{db: DbConnect}
}

func (pges *PgSlotStorage) SetSlot(ctx context.Context, slot *models.Slot) error {
	query := `
		INSERT INTO slots(id)
		VALUES (:id)
	`
	_, err := pges.db.NamedExecContext(ctx, query, map[string]interface{}{
		"id": slot.ID,
	})

	return err
}

func (pges *PgSlotStorage) UpdateSlot(ctx context.Context, slot *models.Slot) (*models.Slot, error) {
	query := `
		UPDATE slots
		SET id = :id
		WHERE id = :id
	`
	_, err := pges.db.NamedExecContext(ctx, query, map[string]interface{}{
		"id": slot.ID,
	})

	return slot, err
}

func (pges *PgSlotStorage) GetSlots(ctx context.Context) ([]*models.Slot, error) {
	query := `
	SELECT id FROM slots
`
	rows, err := pges.db.QueryContext(ctx, query)

	if err != nil {
		logger.ContextLogger.Infof("QueryContext", "err", err.Error())
	}
	defer rows.Close()

	var slots []*models.Slot

	for rows.Next() {
		var slot models.Slot

		if err := rows.Scan(&slot.ID); err != nil {
			logger.ContextLogger.Infof("rowScan", "err", err.Error())
		}

		slots = append(slots, &slot)
	}

	if err := rows.Err(); err != nil {
		logger.ContextLogger.Infof("rowErr", "err", err.Error())
	}

	return slots, nil
}

func (pges *PgSlotStorage) DeleteSlot(ctx context.Context, id int64) error {
	query := `
		DELETE FROM slots
		WHERE id = :id
	`
	_, err := pges.db.NamedExecContext(ctx, query, map[string]interface{}{
		"id": id,
	})

	return err
}
