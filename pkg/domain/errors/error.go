package errors

// ReportError type
type ReportError string

func (ee ReportError) Error() string {
	return string(ee)
}

var (
	ErrOverlaping        = ReportError("another exists for this date")
	ErrReportExist       = ReportError("alredy exist")
	ErrReportNotFound    = ReportError("not found")
	ErrWrangParams       = ReportError("invalid input params")
	ErrConfigWrangParams = ReportError("can not validate config file")
)
