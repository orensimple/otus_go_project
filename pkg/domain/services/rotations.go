//nolint:dupl
package services

import (
	"context"

	"otus_go_project/pkg/domain/interfaces"
	"otus_go_project/pkg/domain/models"
	"otus_go_project/pkg/logger"
)

type RotationService struct {
	RotationStorage    interfaces.RotationStorage
	MemRotationStorage interfaces.MemRotationStorage
}

func (es *RotationService) SetRotation(
	ctx context.Context,
	bannerID,
	slotID int64,
	title string,
) (*models.Rotation, error) {
	rotation := &models.Rotation{
		BannerID: bannerID,
		SlotID:   slotID,
		Title:    title,
	}

	err := es.RotationStorage.SetRotation(ctx, rotation)
	if err != nil {
		return nil, err
	}

	err = es.MemRotationStorage.SetRotation(ctx, slotID, bannerID)
	if err != nil {
		return nil, err
	}

	return rotation, nil
}

func (es *RotationService) UpdateRotation(
	ctx context.Context,
	bannerID,
	slotID int64,
	title string,
) (*models.Rotation, error) {
	rotation := &models.Rotation{
		BannerID: bannerID,
		SlotID:   slotID,
		Title:    title,
	}

	rotation, err := es.RotationStorage.UpdateRotation(ctx, rotation)
	if err != nil {
		return nil, err
	}

	return rotation, nil
}

func (es *RotationService) GetRotations(ctx context.Context) ([]*models.Rotation, error) {
	rotations, err := es.RotationStorage.GetRotations(ctx)
	if err != nil {
		return nil, err
	}

	return rotations, nil
}

func (es *RotationService) DeleteRotation(ctx context.Context, bannerID int64, slotID int64) error {
	err := es.RotationStorage.DeleteRotation(ctx, bannerID, slotID)
	if err != nil {
		return err
	}

	_, err = es.MemRotationStorage.DeleteRotation(ctx, slotID, bannerID)

	return err
}

func (es *RotationService) LoadRotationsFromDbToMem() error {
	rotations, err := es.RotationStorage.GetRotations(context.Background())
	if err != nil {
		return err
	}

	for _, rotation := range rotations {
		err = es.MemRotationStorage.SetRotation(context.Background(), rotation.SlotID, rotation.BannerID)
		if err != nil {
			logger.ContextLogger.Errorf("Can't set rotation to memory storage", rotation, err.Error())
		}
	}

	return nil
}
