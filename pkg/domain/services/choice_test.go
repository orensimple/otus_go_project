package services_test

import (
	"context"
	"reflect"
	"testing"

	"otus_go_project/pkg/domain/errors"
	"otus_go_project/pkg/domain/services"
	"otus_go_project/pkg/memory"

	"github.com/stretchr/testify/assert"
)

//nolint:gomnd,funlen
func TestGetChoices(t *testing.T) {
	memReportStorage := memory.NewMemReportStorage()
	memRotationStorage := memory.NewMemRotationStorage()

	choiceService := &services.ChoiceService{
		MemReportStorage:   memReportStorage,
		MemRotationStorage: memRotationStorage,
	}

	// Choice banner when memReportStorage and memRotationStorage empty
	_, err := choiceService.GetChoice(context.Background(), 1, 2)
	assert.Equal(t, errors.ReportError("not found"), err)

	// Get rotation when memReportStorage and memRotationStorage empty
	_, err = choiceService.MemRotationStorage.GetRotations(context.Background(), int64(1), int64(1))
	assert.Equal(t, errors.ReportError("not found"), err)

	// Get report when memReportStorage and memRotationStorage empty
	reports, err := choiceService.MemReportStorage.GetReports(context.Background())
	assert.Equal(t, nil, err)
	assert.Equal(t, 0, len(reports[1][2])) // len of reports map must be 0

	// Add new rotation with slotID = 1, bannerID = 2
	err = choiceService.MemRotationStorage.SetRotation(context.Background(), 1, 3)
	assert.Equal(t, nil, err) // err must be nil

	// Add new rotation with slotID = 1, bannerID = 2
	err = choiceService.MemRotationStorage.SetRotation(context.Background(), 1, 3)
	assert.Equal(t, nil, err) // err must be nil because one method for set and update rotation

	// Choice banner when memRotationStorage that have one item (bannerID = 2, for slotID =1)
	banner, err := choiceService.GetChoice(context.Background(), 1, 2)
	assert.Equal(t, nil, err)
	assert.Equal(t, int64(3), banner) // bannerID must be 2

	// Get rotation slice from MemRotationStorage
	rotation, err := choiceService.MemRotationStorage.GetRotations(context.Background(), int64(1), int64(2))
	assert.Equal(t, nil, err)              // err must be nil
	assert.Equal(t, 1, len(rotation))      // length of rotation slice must be 1
	assert.Equal(t, int64(3), rotation[0]) // bannerID must be 2 in first rotation slice

	// Get report map from MemReportStorage
	reports, err = choiceService.MemReportStorage.GetReports(context.Background())
	assert.Equal(t, nil, err)                              // err must be nil
	assert.Equal(t, 1, len(reports[1][2]))                 // length of report map must be 1
	assert.Equal(t, int64(0), reports[1][2][3].Conversion) // conversion must be 0, because we not click on the banner
	assert.Equal(t, int64(1), reports[1][2][3].Show)       // show must be 1 because we get choice ones

	// Add ++ conversion to report where slotID = 1, groupID = 1, bannerID = 2
	err = choiceService.MemReportStorage.AddClickToReport(context.Background(), int64(1), int64(2), int64(3))
	assert.Equal(t, nil, err) // err must be nil

	// Add ++ conversion to report where slotID = 1, groupID = 1, bannerID = 3
	err = choiceService.MemReportStorage.AddClickToReport(context.Background(), int64(1), int64(2), int64(1))
	assert.Equal(t, errors.ReportError("not found"), err) // rotation with bannerID = 3 does not exist

	// Get report map from MemReportStorage
	reports, err = choiceService.MemReportStorage.GetReports(context.Background())
	assert.Equal(t, nil, err)                              // err must be nil
	assert.Equal(t, 1, len(reports[1][2]))                 // length of report map must be 1
	assert.Equal(t, int64(1), reports[1][2][3].Conversion) // conversion must be 1, because we add click to the banner
	assert.Equal(t, int64(1), reports[1][2][3].Show)       // show must be 1 because we get choice ones

	var bannersID []int64
	// add 3 rotation for then tests choice new banner
	for i := 4; i < 7; i++ {
		// Add new rotation with slotID = 1, bannerID = {{i}}
		err = choiceService.MemRotationStorage.SetRotation(context.Background(), 1, int64(i))
		assert.Equal(t, nil, err) // err must be nil

		bannersID = append(bannersID, int64(i))
	}

	// get rotation slice from MemRotationStorag
	rotation, err = choiceService.MemRotationStorage.GetRotations(context.Background(), int64(1), int64(1))
	assert.Equal(t, nil, err)         // err must be nil
	assert.Equal(t, 4, len(rotation)) // length of rotation slice must be 4 (1+3)

	// Get report map from MemReportStorage
	reports, err = choiceService.MemReportStorage.GetReports(context.Background())
	assert.Equal(t, nil, err)                              // err must be nil
	assert.Equal(t, 1, len(reports[1][2]))                 // length of report map must be 1
	assert.Equal(t, int64(1), reports[1][2][3].Conversion) // conversion must be 1, because we add click to the banner
	assert.Equal(t, int64(1), reports[1][2][3].Show)       // show must be 1 because we get choice ones

	var choiceBannersID []int64
	// Check that all of 3 new banners returned from GetChoice
	for i := 0; i < 3; i++ {
		banner, err = choiceService.GetChoice(context.Background(), 1, 2)
		assert.Equal(t, nil, err)

		choiceBannersID = append(choiceBannersID, banner)
	}
	assert.Equal(t, true, reflect.DeepEqual(bannersID, choiceBannersID)) // equal new and get banners ID

	// Get report map from MemReportStorage
	reports, err = choiceService.MemReportStorage.GetReports(context.Background())
	assert.Equal(t, nil, err)                              // err must be nil
	assert.Equal(t, 4, len(reports[1][2]))                 // length of report map must be 4
	assert.Equal(t, int64(0), reports[1][2][5].Conversion) // conversion must be 0
	assert.Equal(t, int64(1), reports[1][2][5].Show)       // show must be 1 because we get choice ones

	// Add ++ conversion to report where slotID = 1, groupID = 1, bannerID = 5
	err = choiceService.MemReportStorage.AddClickToReport(context.Background(), int64(1), int64(2), int64(5))
	assert.Equal(t, nil, err) // rotation with bannerID = 5 does not exist
	// Add ++ conversion to report where slotID = 1, groupID = 1, bannerID = 5
	err = choiceService.MemReportStorage.AddClickToReport(context.Background(), int64(1), int64(2), int64(5))
	assert.Equal(t, nil, err) // rotation with bannerID = 5 does not exist

	// Get report map from MemReportStorage
	reports, err = choiceService.MemReportStorage.GetReports(context.Background())
	assert.Equal(t, nil, err)                              // err must be nil
	assert.Equal(t, 4, len(reports[1][2]))                 // length of report map must be 4
	assert.Equal(t, int64(2), reports[1][2][5].Conversion) // conversion must be 1, because we add click to the banner
	assert.Equal(t, int64(1), reports[1][2][5].Show)       // show must be 1 because we get choice ones

	// get rotation slice from MemRotationStorag
	rotation, err = choiceService.MemRotationStorage.GetRotations(context.Background(), int64(1), int64(2))
	assert.Equal(t, nil, err)         // err must be nil
	assert.Equal(t, 4, len(rotation)) // length of rotation slice must be 4 (1+3)

	// Choice banner when memRotationStorage have 4 items, and item where bannerID = 5 have max conversion
	banner2, err := choiceService.GetChoice(context.Background(), 1, 2)
	assert.Equal(t, nil, err)
	assert.Equal(t, int64(5), banner2) // bannerID must be 5*/
}
