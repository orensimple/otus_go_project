//nolint:dupl
package services

import (
	"context"

	"otus_go_project/pkg/domain/interfaces"
	"otus_go_project/pkg/domain/models"
)

type SlotService struct {
	SlotStorage interfaces.SlotStorage
}

func (es *SlotService) SetSlot(ctx context.Context, id int64) (*models.Slot, error) {
	slot := &models.Slot{
		ID: id,
	}

	err := es.SlotStorage.SetSlot(ctx, slot)
	if err != nil {
		return nil, err
	}

	return slot, nil
}

func (es *SlotService) UpdateSlot(ctx context.Context, id int64) (*models.Slot, error) {
	slot := &models.Slot{
		ID: id,
	}

	slot, err := es.SlotStorage.UpdateSlot(ctx, slot)
	if err != nil {
		return nil, err
	}

	return slot, nil
}

func (es *SlotService) GetSlots(ctx context.Context) ([]*models.Slot, error) {
	slots, err := es.SlotStorage.GetSlots(ctx)
	if err != nil {
		return nil, err
	}

	return slots, nil
}

func (es *SlotService) DeleteSlot(ctx context.Context, id int64) error {
	err := es.SlotStorage.DeleteSlot(ctx, id)

	return err
}
