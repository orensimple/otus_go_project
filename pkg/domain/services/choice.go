//nolint:dupl
package services

import (
	"context"
	"math"
	"strconv"
	"time"

	"otus_go_project/pkg/domain/errors"
	"otus_go_project/pkg/domain/interfaces"
	"otus_go_project/pkg/domain/models"
	"otus_go_project/pkg/logger"

	"github.com/spf13/viper"
)

type ChoiceService struct {
	MemReportStorage   interfaces.MemReportStorage
	MemRotationStorage interfaces.MemRotationStorage
	ReportQueue        interfaces.ReportQueue
}

//nolint:funlen
func (es *ChoiceService) GetChoice(ctx context.Context, slotID, groupID int64) (int64, error) {
	bannersIDInSlot, err := es.MemRotationStorage.GetRotations(ctx, slotID, groupID)
	if err != nil {
		return 0, errors.ErrReportNotFound
	}

	reports, err := es.MemReportStorage.GetReports(ctx)
	if err != nil {
		return 0, errors.ErrReportNotFound
	}

	var allShowInSLotGroup int64

	for _, bannerIDInSlot := range bannersIDInSlot {
		bannerStat, ok := reports[slotID][groupID][bannerIDInSlot]
		if !ok {
			err := es.MemReportStorage.AddShowToReport(ctx, slotID, groupID, bannerIDInSlot)
			if err != nil {
				logger.ContextLogger.Infof("Can't add show to report", err.Error())
			}

			err = es.SendShowReport(ctx, slotID, groupID, bannerIDInSlot)
			if err != nil {
				logger.ContextLogger.Infof("Can't send show report", err.Error())
			}

			return bannerIDInSlot, err
		}

		allShowInSLotGroup += bannerStat.Show
	}

	var responseBannerID int64

	maxRatio := -1.0

	for bannerID, bannerStat := range reports[slotID][groupID] {
		currentRation := Mean(bannerStat.Conversion, bannerStat.Show) + HandleFormula(allShowInSLotGroup, bannerStat.Show)

		if currentRation > maxRatio {
			maxRatio = currentRation
			responseBannerID = bannerID
		}
	}

	err = es.MemReportStorage.AddShowToReport(ctx, slotID, groupID, responseBannerID)
	if err != nil {
		logger.ContextLogger.Infof("Can't add show to report", err.Error())
	}

	err = es.SendShowReport(ctx, slotID, groupID, responseBannerID)
	if err != nil {
		logger.ContextLogger.Infof("Can't send show report", err.Error())
	}

	return responseBannerID, err
}

func (es *ChoiceService) AddClickToReport(ctx context.Context, slotID, groupID, bannerID int64) error {
	err := es.MemReportStorage.AddClickToReport(ctx, slotID, groupID, bannerID)
	if err != nil {
		logger.ContextLogger.Infof("Can't add click to report", err.Error())
		return err
	}

	reportShowType, err := strconv.ParseInt(viper.GetString("report_type.conversion"), 10, 64)
	if err != nil {
		logger.ContextLogger.Infof("Can't convert report_type.conversion to int64", err.Error())
	}

	reportQueueFormat := models.ReportQueueFormat{
		Type:     reportShowType,
		BannerID: bannerID,
		GroupID:  groupID,
		SlotID:   slotID,
		Time:     time.Now(),
	}

	err = es.ReportQueue.PublicReport(ctx, reportQueueFormat)
	if err != nil {
		logger.ContextLogger.Infof("Can't send report to queue", err.Error())
	}

	return err
}

func (es *ChoiceService) SendShowReport(ctx context.Context, slotID, groupID, bannerID int64) error {
	if es.ReportQueue == nil {
		return nil
	}

	reportShowType, err := strconv.ParseInt(viper.GetString("report_type.show"), 10, 64)
	if err != nil {
		logger.ContextLogger.Infof("Can't convert report_type.show to int64", err.Error())
	}

	reportQueueFormat := models.ReportQueueFormat{
		Type:     reportShowType,
		BannerID: bannerID,
		GroupID:  groupID,
		SlotID:   slotID,
		Time:     time.Now(),
	}

	err = es.ReportQueue.PublicReport(ctx, reportQueueFormat)
	if err != nil {
		logger.ContextLogger.Infof("Can't public report", err.Error())
	}

	return err
}

func Mean(conversion, show int64) float64 {
	return float64(conversion) / float64(show)
}

func HandleFormula(allShowInSLotGroup, show int64) float64 {
	return math.Sqrt(math.Log(float64(allShowInSLotGroup)) / (2 * float64(show)))
}
