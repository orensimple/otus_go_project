//nolint:dupl
package services

import (
	"context"

	"otus_go_project/pkg/domain/interfaces"
	"otus_go_project/pkg/domain/models"
)

type GroupService struct {
	GroupStorage interfaces.GroupStorage
}

func (es *GroupService) SetGroup(ctx context.Context, id int64) (*models.Group, error) {
	group := &models.Group{
		ID: id,
	}

	err := es.GroupStorage.SetGroup(ctx, group)
	if err != nil {
		return nil, err
	}

	return group, nil
}

func (es *GroupService) UpdateGroup(ctx context.Context, id int64) (*models.Group, error) {
	group := &models.Group{
		ID: id,
	}

	group, err := es.GroupStorage.UpdateGroup(ctx, group)
	if err != nil {
		return nil, err
	}

	return group, nil
}

func (es *GroupService) GetGroups(ctx context.Context) ([]*models.Group, error) {
	groups, err := es.GroupStorage.GetGroups(ctx)
	if err != nil {
		return nil, err
	}

	return groups, nil
}

func (es *GroupService) DeleteGroup(ctx context.Context, id int64) error {
	err := es.GroupStorage.DeleteGroup(ctx, id)

	return err
}
