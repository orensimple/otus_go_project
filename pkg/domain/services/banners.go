//nolint:dupl
package services

import (
	"context"

	"otus_go_project/pkg/domain/interfaces"
	"otus_go_project/pkg/domain/models"
)

type BannerService struct {
	BannerStorage      interfaces.BannerStorage
	MemRotationStorage interfaces.MemRotationStorage
}

func (es *BannerService) SetBanner(ctx context.Context, id int64, title string) (*models.Banner, error) {
	banner := &models.Banner{
		ID:    id,
		Title: title,
	}

	err := es.BannerStorage.SetBanner(ctx, banner)
	if err != nil {
		return nil, err
	}

	return banner, nil
}

func (es *BannerService) UpdateBanner(ctx context.Context, id int64, title string) (*models.Banner, error) {
	banner := &models.Banner{
		ID:    id,
		Title: title,
	}

	banner, err := es.BannerStorage.UpdateBanner(ctx, banner)
	if err != nil {
		return nil, err
	}

	return banner, nil
}

func (es *BannerService) GetBanners(ctx context.Context) ([]*models.Banner, error) {
	banners, err := es.BannerStorage.GetBanners(ctx)
	if err != nil {
		return nil, err
	}

	return banners, nil
}

func (es *BannerService) DeleteBanner(ctx context.Context, id int64) error {
	err := es.BannerStorage.DeleteBanner(ctx, id)

	return err
}
