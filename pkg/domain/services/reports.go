//nolint:dupl
package services

import (
	"context"

	"otus_go_project/pkg/domain/interfaces"
	"otus_go_project/pkg/domain/models"
	"otus_go_project/pkg/logger"
)

type ReportService struct {
	ReportStorage    interfaces.ReportStorage
	MemReportStorage interfaces.MemReportStorage
}

func (es *ReportService) SetReport(
	ctx context.Context,
	bannerID,
	groupID,
	slotID,
	show,
	conversion int64,
) (*models.Report, error) {
	report := &models.Report{
		BannerID:   bannerID,
		GroupID:    groupID,
		SlotID:     slotID,
		Show:       show,
		Conversion: conversion,
	}

	err := es.ReportStorage.SetReport(ctx, report)
	if err != nil {
		return nil, err
	}

	return report, nil
}

func (es *ReportService) UpdateReport(
	ctx context.Context,
	bannerID,
	groupID,
	slotID,
	show,
	conversion int64,
) (*models.Report, error) {
	report := &models.Report{
		BannerID:   bannerID,
		GroupID:    groupID,
		SlotID:     slotID,
		Show:       show,
		Conversion: conversion,
	}

	report, err := es.ReportStorage.UpdateReport(ctx, report)
	if err != nil {
		return nil, err
	}

	return report, nil
}

func (es *ReportService) GetReports(ctx context.Context) ([]*models.Report, error) {
	reports, err := es.ReportStorage.GetReports(ctx)
	if err != nil {
		return nil, err
	}

	return reports, nil
}

func (es *ReportService) DeleteReport(ctx context.Context, bannerID int64, groupID int64, slotID int64) error {
	err := es.ReportStorage.DeleteReport(ctx, bannerID, groupID, slotID)

	return err
}

func (es *ReportService) AutoUpdateRotations(ctx context.Context) error {
	reports, err := es.MemReportStorage.GetReports(ctx)
	if err != nil {
		return err
	}

	var requestReports []*models.Report

	for slotID, groups := range reports {
		for groupID, banners := range groups {
			for bannerID, stats := range banners {
				report := &models.Report{
					BannerID:   bannerID,
					SlotID:     slotID,
					GroupID:    groupID,
					Conversion: stats.Conversion,
					Show:       stats.Show,
				}
				requestReports = append(requestReports, report)
			}
		}
	}

	err = es.ReportStorage.AutoUpdateReports(ctx, requestReports)
	if err != nil {
		logger.ContextLogger.Errorf("Can't update report", err.Error())
	}

	return err
}

func (es *ReportService) LoadReportsFromDbToMem() error {
	reports, err := es.ReportStorage.GetReports(context.Background())
	if err != nil {
		return err
	}

	for _, report := range reports {
		err = es.MemReportStorage.SetReport(context.Background(), report)
		if err != nil {
			logger.ContextLogger.Errorf("Can't set report to memory storage", report, err.Error())
		}
	}

	return nil
}
