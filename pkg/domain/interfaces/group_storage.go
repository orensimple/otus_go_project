package interfaces

import (
	"context"

	"otus_go_project/pkg/domain/models"
)

// GroupStorage interface
type GroupStorage interface {
	SetGroup(ctx context.Context, group *models.Group) error
	UpdateGroup(ctx context.Context, group *models.Group) (*models.Group, error)
	GetGroups(ctx context.Context) ([]*models.Group, error)
	DeleteGroup(ctx context.Context, ID int64) error
}
