package interfaces

import (
	"context"

	"otus_go_project/pkg/domain/models"
)

// ReportQueue interface
type ReportQueue interface {
	PublicReport(ctx context.Context, report models.ReportQueueFormat) error
}
