package interfaces

import (
	"context"

	"otus_go_project/pkg/domain/models"
)

// BannerStorage interface
type BannerStorage interface {
	SetBanner(ctx context.Context, banner *models.Banner) error
	UpdateBanner(ctx context.Context, banner *models.Banner) (*models.Banner, error)
	GetBanners(ctx context.Context) ([]*models.Banner, error)
	DeleteBanner(ctx context.Context, ID int64) error
}
