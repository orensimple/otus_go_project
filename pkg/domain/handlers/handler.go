//nolint:dupl
package handlers

import (
	"net/http"

	"otus_go_project/pkg/storage"
)

type Handler struct {
	Handlers *http.Handler
	Storages *storage.Enabled
}

const (
	SuccessMessageConst     = "success"
	SuccessResponseCode     = 200
	WrongParamsResponseCode = 400
	NotFoundResponseCode    = 404
)
