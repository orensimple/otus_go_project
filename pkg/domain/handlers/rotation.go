//nolint:dupl
package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"otus_go_project/pkg/domain/models"
	"otus_go_project/pkg/logger"
)

func (h *Handler) SetRotation(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Set("Content-Type", "application/json; charset=utf-8")

	data := make(map[string]interface{})

	bannerID, slotID, title, err := validateRotation(req)
	if err != nil {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	banner, err := h.Storages.MainRotationService.SetRotation(req.Context(), bannerID, slotID, title)
	if err == nil {
		data["result"] = banner
	} else {
		data["error"] = err.Error()
	}

	resp.WriteHeader(SuccessResponseCode)

	err = json.NewEncoder(resp).Encode(data)
	if err != nil {
		logger.ContextLogger.Errorf("Failed to encode data", err.Error())
	}
}

func (h *Handler) GetRotations(resp http.ResponseWriter, req *http.Request) {
	data := make(map[string][]*models.Rotation)
	result, _ := h.Storages.MainRotationService.GetRotations(req.Context())
	data["result"] = result
	response, _ := json.Marshal(data)

	resp.Header().Set("Content-Type", "application/json; charset=utf-8")
	resp.WriteHeader(SuccessResponseCode)

	_, err := resp.Write(response)
	if err != nil {
		logger.ContextLogger.Errorf("Failed to write response", err.Error())
	}
}

func (h *Handler) UpdateRotation(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Set("Content-Type", "application/json; charset=utf-8")

	data := make(map[string]interface{})

	bannerID, slotID, title, err := validateRotation(req)
	if err != nil {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	banner, err := h.Storages.MainRotationService.UpdateRotation(req.Context(), bannerID, slotID, title)
	if err == nil {
		data["result"] = banner
	} else {
		data["error"] = err.Error()
	}

	resp.WriteHeader(SuccessResponseCode)

	err = json.NewEncoder(resp).Encode(data)
	if err != nil {
		logger.ContextLogger.Errorf("Failed to encode data", err.Error())
	}
}

func (h *Handler) DeleteRotation(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Set("Content-Type", "application/json; charset=utf-8")

	data := make(map[string]interface{})

	err := req.ParseForm()
	if err != nil {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	bannerID, _ := strconv.ParseInt(req.Form.Get("banner_id"), 10, 64)
	if bannerID == 0 {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	slotID, _ := strconv.ParseInt(req.Form.Get("slot_id"), 10, 64)
	if slotID == 0 {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	err = h.Storages.MainRotationService.DeleteRotation(req.Context(), bannerID, slotID)
	if err == nil {
		data["result"] = SuccessMessageConst
	} else {
		data["error"] = err.Error()
	}

	resp.WriteHeader(SuccessResponseCode)

	err = json.NewEncoder(resp).Encode(data)
	if err != nil {
		logger.ContextLogger.Errorf("Failed to encode data", err.Error())
	}
}

func validateRotation(req *http.Request) (int64, int64, string, error) {
	err := req.ParseForm()
	if err != nil {
		logger.ContextLogger.Infof("form", "uri", err)
		return 0, 0, "", err
	}

	bannerID, _ := strconv.ParseInt(req.Form.Get("banner_id"), 10, 64)
	if bannerID == 0 {
		logger.ContextLogger.Infof("bannerID", "uri", err)
		return 0, 0, "", err
	}

	slotID, _ := strconv.ParseInt(req.Form.Get("slot_id"), 10, 64)
	if slotID == 0 {
		logger.ContextLogger.Infof("slotID", "uri", err)
		return 0, 0, "", err
	}

	title := req.Form.Get("title")
	if len(title) == 0 {
		logger.ContextLogger.Infof("title", "uri", err)
		return 0, 0, "", err
	}

	return bannerID, slotID, title, err
}
