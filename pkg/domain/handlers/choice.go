//nolint:dupl
package handlers

import (
	"encoding/json"
	"net/http"
	"otus_go_project/pkg/logger"
	"strconv"
)

func (h *Handler) GetChoice(resp http.ResponseWriter, req *http.Request) {
	data := make(map[string]interface{})

	err := req.ParseForm()
	if err != nil {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	groupID, _ := strconv.ParseInt(req.Form.Get("group_id"), 10, 64)
	if groupID == 0 {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	slotID, _ := strconv.ParseInt(req.Form.Get("slot_id"), 10, 64)
	if slotID == 0 {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	result, err := h.Storages.MainChoiceService.GetChoice(req.Context(), slotID, groupID)
	if err != nil {
		resp.WriteHeader(NotFoundResponseCode)
		return
	}

	data["result"] = result
	response, _ := json.Marshal(data)

	resp.Header().Set("Content-Type", "application/json; charset=utf-8")
	resp.WriteHeader(SuccessResponseCode)

	_, err = resp.Write(response)
	if err != nil {
		logger.ContextLogger.Errorf("Failed to write response", err.Error())
	}
}

func (h *Handler) ClickChoice(resp http.ResponseWriter, req *http.Request) {
	data := make(map[string]interface{})

	err := req.ParseForm()
	if err != nil {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	groupID, _ := strconv.ParseInt(req.Form.Get("group_id"), 10, 64)
	if groupID == 0 {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	slotID, _ := strconv.ParseInt(req.Form.Get("slot_id"), 10, 64)
	if slotID == 0 {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	bannerID, _ := strconv.ParseInt(req.Form.Get("banner_id"), 10, 64)
	if bannerID == 0 {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	err = h.Storages.MainChoiceService.AddClickToReport(req.Context(), slotID, groupID, bannerID)
	if err != nil {
		resp.WriteHeader(NotFoundResponseCode)
		return
	}

	data["result"] = SuccessMessageConst
	response, _ := json.Marshal(data)

	resp.Header().Set("Content-Type", "application/json; charset=utf-8")
	resp.WriteHeader(SuccessResponseCode)

	_, err = resp.Write(response)
	if err != nil {
		logger.ContextLogger.Errorf("Failed to write response", err.Error())
	}
}
