//nolint:dupl
package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"otus_go_project/pkg/domain/models"
	"otus_go_project/pkg/logger"
)

func (h *Handler) SetReport(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Set("Content-Type", "application/json; charset=utf-8")

	data := make(map[string]interface{})

	bannerID, groupID, slotID, show, conversion, err := validateReport(req)
	if err != nil {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	report, err := h.Storages.MainReportService.SetReport(req.Context(), bannerID, groupID, slotID, show, conversion)
	if err == nil {
		data["result"] = report
	} else {
		data["error"] = err.Error()
	}

	resp.WriteHeader(SuccessResponseCode)

	err = json.NewEncoder(resp).Encode(data)
	if err != nil {
		logger.ContextLogger.Errorf("Failed to encode data", err.Error())
	}
}

func (h *Handler) GetReports(resp http.ResponseWriter, req *http.Request) {
	data := make(map[string][]*models.Report)
	result, _ := h.Storages.MainReportService.GetReports(req.Context())
	data["result"] = result
	response, _ := json.Marshal(data)

	resp.Header().Set("Content-Type", "application/json; charset=utf-8")
	resp.WriteHeader(SuccessResponseCode)

	_, err := resp.Write(response)
	if err != nil {
		logger.ContextLogger.Errorf("Failed to write response", err.Error())
	}
}

func (h *Handler) UpdateReport(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Set("Content-Type", "application/json; charset=utf-8")

	data := make(map[string]interface{})

	bannerID, groupID, slotID, show, conversion, err := validateReport(req)
	if err != nil {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	report, err := h.Storages.MainReportService.UpdateReport(req.Context(), bannerID, groupID, slotID, show, conversion)
	if err == nil {
		data["result"] = report
	} else {
		data["error"] = err.Error()
	}

	resp.WriteHeader(SuccessResponseCode)

	err = json.NewEncoder(resp).Encode(data)
	if err != nil {
		logger.ContextLogger.Errorf("Failed to encode data", err.Error())
	}
}

func (h *Handler) DeleteReport(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Set("Content-Type", "application/json; charset=utf-8")

	data := make(map[string]interface{})

	err := req.ParseForm()
	if err != nil {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	bannerID, _ := strconv.ParseInt(req.Form.Get("banner_id"), 10, 64)
	if bannerID == 0 {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	groupID, _ := strconv.ParseInt(req.Form.Get("group_id"), 10, 64)
	if groupID == 0 {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	slotID, _ := strconv.ParseInt(req.Form.Get("slotID"), 10, 64)
	if slotID == 0 {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	err = h.Storages.MainReportService.DeleteReport(req.Context(), bannerID, groupID, slotID)
	if err == nil {
		data["result"] = SuccessMessageConst
	} else {
		data["error"] = err.Error()
	}

	resp.WriteHeader(SuccessResponseCode)

	err = json.NewEncoder(resp).Encode(data)
	if err != nil {
		logger.ContextLogger.Errorf("Failed to encode data", err.Error())
	}
}

func validateReport(req *http.Request) (int64, int64, int64, int64, int64, error) {
	err := req.ParseForm()
	if err != nil {
		logger.ContextLogger.Infof("form", "uri", err)
		return 0, 0, 0, 0, 0, err
	}

	bannerID, _ := strconv.ParseInt(req.Form.Get("banner_id"), 10, 64)
	if bannerID == 0 {
		logger.ContextLogger.Infof("banner_id", "uri", err)
		return 0, 0, 0, 0, 0, err
	}

	groupID, _ := strconv.ParseInt(req.Form.Get("group_id"), 10, 64)
	if groupID == 0 {
		logger.ContextLogger.Infof("group_id", "uri", err)
		return 0, 0, 0, 0, 0, err
	}

	slotID, _ := strconv.ParseInt(req.Form.Get("slot_id"), 10, 64)
	if slotID == 0 {
		logger.ContextLogger.Infof("slotID", "uri", err)
		return 0, 0, 0, 0, 0, err
	}

	show, _ := strconv.ParseInt(req.Form.Get("show"), 10, 64)
	if show == 0 {
		logger.ContextLogger.Infof("show", "uri", err)
		return 0, 0, 0, 0, 0, err
	}

	conversion, _ := strconv.ParseInt(req.Form.Get("conversion"), 10, 64)
	if conversion == 0 {
		logger.ContextLogger.Infof("conversion", "uri", err)
		return 0, 0, 0, 0, 0, err
	}

	return bannerID, groupID, slotID, show, conversion, err
}
