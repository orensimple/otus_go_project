//nolint:dupl
package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"otus_go_project/pkg/domain/models"
	"otus_go_project/pkg/logger"
)

func (h *Handler) SetGroup(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Set("Content-Type", "application/json; charset=utf-8")

	data := make(map[string]interface{})

	id, err := validateGroup(req)
	if err != nil {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	banner, err := h.Storages.MainGroupService.SetGroup(req.Context(), id)
	if err == nil {
		data["result"] = banner
	} else {
		data["error"] = err.Error()
	}

	resp.WriteHeader(SuccessResponseCode)

	err = json.NewEncoder(resp).Encode(data)
	if err != nil {
		logger.ContextLogger.Errorf("Failed to encode data", err.Error())
	}
}

func (h *Handler) GetGroups(resp http.ResponseWriter, req *http.Request) {
	data := make(map[string][]*models.Group)
	result, _ := h.Storages.MainGroupService.GetGroups(req.Context())
	data["result"] = result
	response, _ := json.Marshal(data)

	resp.Header().Set("Content-Type", "application/json; charset=utf-8")
	resp.WriteHeader(SuccessResponseCode)

	_, err := resp.Write(response)
	if err != nil {
		logger.ContextLogger.Errorf("Failed to write response", err.Error())
	}
}

func (h *Handler) UpdateGroup(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Set("Content-Type", "application/json; charset=utf-8")

	data := make(map[string]interface{})

	id, err := validateGroup(req)
	if err != nil {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	banner, err := h.Storages.MainGroupService.UpdateGroup(req.Context(), id)
	if err == nil {
		data["result"] = banner
	} else {
		data["error"] = err.Error()
	}

	resp.WriteHeader(SuccessResponseCode)

	err = json.NewEncoder(resp).Encode(data)
	if err != nil {
		logger.ContextLogger.Errorf("Failed to encode data", err.Error())
	}
}

func (h *Handler) DeleteGroup(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Set("Content-Type", "application/json; charset=utf-8")

	data := make(map[string]interface{})

	err := req.ParseForm()
	if err != nil {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	id, _ := strconv.ParseInt(req.Form.Get("id"), 10, 64)
	if id == 0 {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	err = h.Storages.MainGroupService.DeleteGroup(req.Context(), id)
	if err == nil {
		data["result"] = SuccessMessageConst
	} else {
		data["error"] = err.Error()
	}

	resp.WriteHeader(SuccessResponseCode)

	err = json.NewEncoder(resp).Encode(data)
	if err != nil {
		logger.ContextLogger.Errorf("Failed to encode data", err.Error())
	}
}

func validateGroup(req *http.Request) (int64, error) {
	err := req.ParseForm()
	if err != nil {
		logger.ContextLogger.Infof("form", "uri", err)
		return 0, err
	}

	id, _ := strconv.ParseInt(req.Form.Get("id"), 10, 64)
	if id == 0 {
		logger.ContextLogger.Infof("id", "uri", err)
		return 0, err
	}

	return id, err
}
