//nolint:dupl
package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"otus_go_project/pkg/domain/models"
	"otus_go_project/pkg/logger"
)

func (h *Handler) SetBanner(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Set("Content-Type", "application/json; charset=utf-8")

	data := make(map[string]interface{})

	id, title, err := validateBanner(req)
	if err != nil {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	banner, err := h.Storages.MainBannerService.SetBanner(req.Context(), id, title)
	if err == nil {
		data["result"] = banner
	} else {
		data["error"] = err.Error()
	}

	resp.WriteHeader(SuccessResponseCode)

	err = json.NewEncoder(resp).Encode(data)
	if err != nil {
		logger.ContextLogger.Errorf("Failed to encode data", err.Error())
	}
}

func (h *Handler) GetBanners(resp http.ResponseWriter, req *http.Request) {
	data := make(map[string][]*models.Banner)
	result, _ := h.Storages.MainBannerService.GetBanners(req.Context())
	data["result"] = result
	response, _ := json.Marshal(data)

	resp.Header().Set("Content-Type", "application/json; charset=utf-8")
	resp.WriteHeader(SuccessResponseCode)

	_, err := resp.Write(response)
	if err != nil {
		logger.ContextLogger.Errorf("Failed to write response", err.Error())
	}
}

func (h *Handler) UpdateBanner(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Set("Content-Type", "application/json; charset=utf-8")

	data := make(map[string]interface{})

	id, title, err := validateBanner(req)
	if err != nil {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	banner, err := h.Storages.MainBannerService.UpdateBanner(req.Context(), id, title)
	if err == nil {
		data["result"] = banner
	} else {
		data["error"] = err.Error()
	}

	resp.WriteHeader(SuccessResponseCode)

	err = json.NewEncoder(resp).Encode(data)
	if err != nil {
		logger.ContextLogger.Errorf("Failed to encode data", err.Error())
	}
}

func (h *Handler) DeleteBanner(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Set("Content-Type", "application/json; charset=utf-8")

	data := make(map[string]interface{})

	err := req.ParseForm()
	if err != nil {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	id, _ := strconv.ParseInt(req.Form.Get("id"), 10, 64)
	if id == 0 {
		resp.WriteHeader(WrongParamsResponseCode)
		return
	}

	err = h.Storages.MainBannerService.DeleteBanner(req.Context(), id)
	if err == nil {
		data["result"] = SuccessMessageConst
	} else {
		data["error"] = err.Error()
	}

	resp.WriteHeader(SuccessResponseCode)

	err = json.NewEncoder(resp).Encode(data)
	if err != nil {
		logger.ContextLogger.Errorf("Failed to encode data", err.Error())
	}
}

func validateBanner(req *http.Request) (int64, string, error) {
	err := req.ParseForm()
	if err != nil {
		logger.ContextLogger.Infof("form", "uri", err)
		return 0, "", err
	}

	id, _ := strconv.ParseInt(req.Form.Get("id"), 10, 64)
	if id == 0 {
		logger.ContextLogger.Infof("id", "uri", err)
		return 0, "", err
	}

	title := req.Form.Get("title")
	if len(title) == 0 {
		logger.ContextLogger.Infof("title", "uri", err)
		return 0, "", err
	}

	return id, title, err
}
