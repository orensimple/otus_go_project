package storage

import (
	"fmt"
	"os"

	"otus_go_project/pkg/domain/services"
	"otus_go_project/pkg/logger"
	"otus_go_project/pkg/maindb"
	"otus_go_project/pkg/memory"
	"otus_go_project/pkg/queue"

	"github.com/spf13/viper"
)

type Enabled struct {
	MainBannerService   *services.BannerService
	MainGroupService    *services.GroupService
	MainReportService   *services.ReportService
	MainRotationService *services.RotationService
	MainSlotService     *services.SlotService
	MainChoiceService   *services.ChoiceService
}

func (e *Enabled) InitStorages() {
	dsn := GetDsn()

	err := maindb.InitDbConnect(dsn)
	if err != nil {
		logger.ContextLogger.Errorf("DB connection not available, stop app", err.Error())
		os.Exit(1) //nolint:gomnd
	}

	e.InitDbServices()
	e.InitChoiceReportService()
}

func (e *Enabled) InitDbServices() {
	maindbBannerStorage := maindb.NewPgBannerStorage()

	e.MainBannerService = &services.BannerService{
		BannerStorage: maindbBannerStorage,
	}

	maindbSlotStorage := maindb.NewPgSlotStorage()

	e.MainSlotService = &services.SlotService{
		SlotStorage: maindbSlotStorage,
	}

	maindbGroupStorage := maindb.NewPgGroupStorage()

	e.MainGroupService = &services.GroupService{
		GroupStorage: maindbGroupStorage,
	}
}

func (e *Enabled) InitChoiceReportService() {
	memReportStorage := memory.NewMemReportStorage()
	memRotationStorage := memory.NewMemRotationStorage()

	var err error

	reportQueue, err := queue.NewReportQueue()
	if err != nil {
		logger.ContextLogger.Errorf("Problem connect to RabbitMQ", err.Error())
	}

	e.MainChoiceService = &services.ChoiceService{
		MemReportStorage:   memReportStorage,
		MemRotationStorage: memRotationStorage,
		ReportQueue:        reportQueue,
	}

	maindbReportStorage := maindb.NewPgReportStorage()

	e.MainReportService = &services.ReportService{
		ReportStorage:    maindbReportStorage,
		MemReportStorage: memReportStorage,
	}

	maindbRotationStorage := maindb.NewPgRotationStorage()

	e.MainRotationService = &services.RotationService{
		RotationStorage:    maindbRotationStorage,
		MemRotationStorage: memRotationStorage,
	}
}

func GetDsn() string {
	return fmt.Sprintf(
		"postgres://%s:%s@%s:%s/%s",
		viper.GetString("postgres.user"),
		viper.GetString("postgres.passwd"),
		viper.GetString("postgres.ip"),
		viper.GetString("postgres.port"),
		viper.GetString("postgres.db"),
	)
}
